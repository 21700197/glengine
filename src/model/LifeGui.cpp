#include "model/LifeGui.hpp"


/**
 * @brief Constructs a new LifeGui object
 * LifeGui default constructor, taking a rendering program
 * @param program The program used to render the LifeGui GUI element
 */
LifeGui::LifeGui(Program &program) 
		: GuiElement(program),
		  hp_3_texture(new Texture(GL_TEXTURE_2D)),
		  hp_2_texture(new Texture(GL_TEXTURE_2D)),
		  hp_1_texture(new Texture(GL_TEXTURE_2D)) {
	stbi_set_flip_vertically_on_load(true);

	Image<> rgbMapImage;
	std::string rgbFilename = absolutename("meshes/life3.png");
	rgbMapImage.data = stbi_load(rgbFilename.c_str(), &rgbMapImage.width, &rgbMapImage.height, &rgbMapImage.channels, STBI_default);
	this->hp_3_texture->setData(rgbMapImage, true);

	rgbFilename = absolutename("meshes/life2.png");
	rgbMapImage.data = stbi_load(rgbFilename.c_str(), &rgbMapImage.width, &rgbMapImage.height, &rgbMapImage.channels, STBI_default);
	this->hp_2_texture->setData(rgbMapImage, true);

	rgbFilename = absolutename("meshes/life1.png");
	rgbMapImage.data = stbi_load(rgbFilename.c_str(), &rgbMapImage.width, &rgbMapImage.height, &rgbMapImage.channels, STBI_default);
	this->hp_1_texture->setData(rgbMapImage, true);
	
	stbi_set_flip_vertically_on_load(false);

	setTexture(this->hp_3_texture);
}

/**
 * @brief Method setting the current life amount
 * Method setting how much health points the GUI element should be representing
 * @param life 
 */
void LifeGui::setCurrentLife(const unsigned int &life) {
	if(life >= 3) {
		this->setTexture(this->hp_3_texture);
    	this->setDimension({0.15, 0.05});
	} else if(life == 2) {
		this->setTexture(this->hp_2_texture);
    	this->setDimension({0.10, 0.05});
	} else if(life <= 1) {
		this->setTexture(this->hp_1_texture);
    	this->setDimension({0.05, 0.05});
	}
}
