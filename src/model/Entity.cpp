#include "model/Entity.hpp"


#include <array>


#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/quaternion.hpp"


/**
 * @brief Construct a new Entity object
 * Entity default constructor, placing the entity at the center of the 3D world, facing Y front, Unscaled, with world-aligned local axis 
 */
Entity::Entity() 
        : m_position(glm::vec3(0)),
          m_rotation(glm::quat(0, 1, 0, 0)),
          m_scale(1), m_localX(1, 0, 0),
          m_localY(0, 1, 0), m_localZ(0, 0, 1) {}

/**
 * @brief Update method supposed to be called each frame
 * Overridable virtual update method used to update any entity's state each frame
 * @param time time elapsed since last loop
 */
void Entity::update(const float &time) {}

/**
 * @brief Method used to move the entity
 * Method translating the entity's current position by adding a translation vector to it, then computing the model matrix
 * @param translation The translation vector
 */
void Entity::move(glm::vec3 translation) {
    this->m_position += translation;

    this->computeModelMatrix();
}

/**
 * @brief Method used to rotate the entity
 * Method rotating the entity's current position by using a quaternion, then computing the model matrix
 * @param rotation The rotation quaternion
 */
void Entity::rotate(glm::quat rotation) {
    this->m_localX = normalize(rotation * this->m_localX);
    this->m_localY = normalize(rotation * this->m_localY);
    this->m_localZ = normalize(rotation * this->m_localZ);

    this->m_rotation = rotation * this->m_rotation;

    this->computeModelMatrix();
}

/**
 * @brief Method used to scale the entity by a single value
 * method scaling the entity by creating a vector with all 3 components being equal to a single scale factor, then computing the model matrix
 * @param scale The single scale factor
 */
void Entity::setScale(float scale) {
    this->m_scale = glm::vec3(scale);

    this->computeModelMatrix();
}

/**
 * @brief Method used to scale the entity by a value on each axis
 * Method scaling the entity by a given value on each axis, then computing the model matrix
 * @param scales 
 */
void Entity::setScale(glm::vec3 scales) {
    this->m_scale = scales;

    this->computeModelMatrix();
}

/**
 * @brief Method computing the entity's OBB's model matrix
 * Method using the entity's corresponding 3D model's renderObject to compuute it's OBB's model matrix for in-world display
 * @param renderObject The entity's 3D model's RenderObject
 * @return glm::mat4 The entity's bounding box's model matrix
 */
glm::mat4 Entity::get_box_model_matrix(std::unique_ptr<RenderObject> &renderObject) const {
    glm::vec3 boxDims = renderObject->get_dimensions();

    glm::vec3 scaleVec(
        boxDims.x * this->m_scale.x,
        boxDims.y * this->m_scale.y,
        boxDims.z * this->m_scale.z
    );

    glm::mat4 unitTranslateMatrix = glm::translate(glm::mat4(1), this->m_position);
    glm::mat4 unitScaleMatrix     = glm::scale(glm::mat4(1), scaleVec);
    glm::mat4 unitRotationMatrix  = glm::mat4_cast(this->m_rotation);

    return unitTranslateMatrix * unitRotationMatrix * unitScaleMatrix; 
}

/**
 * @brief Method computing the entity's model matrix for in-world display
 * Method using the entity's position, rotation and scale to compute it's model matrix for in-world display
 */
void Entity::computeModelMatrix() {
    glm::mat4 translateMatrix = glm::translate(glm::mat4(1), this->m_position);
    glm::mat4 scaleMatrix     = glm::scale(glm::mat4(1), this->m_scale);
    glm::mat4 rotationMatrix  = glm::mat4_cast(this->m_rotation);

    this->m_modelMatrix = translateMatrix * rotationMatrix * scaleMatrix;
}

/**
 * @brief Method returning wether the object is colliding with another one
 * Method returnning wether or not the object is colliding with another, given an other entity and both entities' corresponding renderObjects.
 * If `movingObjects` is set to true, the method also take in account the two objects' velocities
 * @param renderObjectThis The entity's corresponding renderObject
 * @param other The other entity
 * @param renderObjectOther The other entity's render object
 * @param movingObjects Wether or not the object should take velocities in account
 * @param velocityThis The entity's velocity
 * @param velocityOther The other entity's velocity
 * @return true if ther is a collision with the other object
 * @return false if ther is no collision with the other object
 */
bool Entity::check_box_intersection(const std::unique_ptr<RenderObject> &renderObjectThis,
                                    const Entity &other,
                                    const std::unique_ptr<RenderObject> &renderObjectOther,
                                    const bool &movingObjects,
                                    const glm::vec3 &velocityThis,
                                    const glm::vec3 &velocityOther) const {
    glm::vec3 boxDimsThis  = renderObjectThis->get_dimensions();
    glm::vec3 boxDimsOther = renderObjectOther->get_dimensions(); 
    
    glm::vec3 centerThis  = this->m_position;
    glm::vec3 centerOther = other.m_position;

    std::array<glm::vec3, 3> axisThis {
        glm::normalize(this->get_x_axis()),
        glm::normalize(this->get_y_axis()),
        glm::normalize(this->get_z_axis())
    };
    std::array<glm::vec3, 3> axisOther {
        glm::normalize(other.get_x_axis()),
        glm::normalize(other.get_y_axis()),
        glm::normalize(other.get_z_axis())
    };

    std::array<float, 3> extentsThis {
        boxDimsThis.x * this->m_scale.x,
        boxDimsThis.y * this->m_scale.y,
        boxDimsThis.z * this->m_scale.z
    };
    std::array<float, 3> extentsOther {
        boxDimsOther.x * other.m_scale.x,
        boxDimsOther.y * other.m_scale.y,
        boxDimsOther.z * other.m_scale.z
    };

    glm::mat3 cMatrix {
        {glm::dot(axisThis[0], axisOther[0]), glm::dot(axisThis[0], axisOther[1]), glm::dot(axisThis[0], axisOther[2])},
        {glm::dot(axisThis[1], axisOther[0]), glm::dot(axisThis[1], axisOther[1]), glm::dot(axisThis[1], axisOther[2])},
        {glm::dot(axisThis[2], axisOther[0]), glm::dot(axisThis[2], axisOther[1]), glm::dot(axisThis[2], axisOther[2])}
    };

    glm::vec3 centersDistance = centerOther - centerThis;

    if (extentsThis[0] + (extentsOther[0]*glm::length(cMatrix[0][0]) + extentsOther[1]*glm::length(cMatrix[0][1]) + extentsOther[2]*glm::length(cMatrix[0][2])) < glm::length(abs(glm::dot(axisThis[0], centersDistance)))) {
        return false;
    }

    if (extentsThis[1] + (extentsOther[0]*glm::length(cMatrix[1][0]) + extentsOther[1]*glm::length(cMatrix[1][1]) + extentsOther[2]*glm::length(cMatrix[1][2])) < glm::length(abs(glm::dot(axisThis[1], centersDistance)))) {
        return false;
    }

    if (extentsThis[2] + (extentsOther[0]*glm::length(cMatrix[2][0]) + extentsOther[1]*glm::length(cMatrix[2][1]) + extentsOther[2]*glm::length(cMatrix[2][2])) < glm::length(abs(glm::dot(axisThis[2], centersDistance)))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[0][0]) + extentsThis[1]*glm::length(cMatrix[1][0]) + extentsThis[2]*glm::length(cMatrix[2][0]) + extentsOther[0] < glm::length(abs(glm::dot(axisOther[0], centersDistance)))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[0][1]) + extentsThis[1]*glm::length(cMatrix[1][1]) + extentsThis[2]*glm::length(cMatrix[2][1]) + extentsOther[1] < glm::length(abs(glm::dot(axisOther[1], centersDistance)))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[0][2]) + extentsThis[1]*glm::length(cMatrix[1][2]) + extentsThis[2]*glm::length(cMatrix[2][2]) + extentsOther[2] < glm::length(abs(glm::dot(axisOther[2], centersDistance)))) {
        return false;
    }

    if (extentsThis[1]*glm::length(cMatrix[2][0]) + extentsThis[2]*glm::length(cMatrix[1][0]) + extentsOther[1]*glm::length(cMatrix[0][2]) + extentsOther[2]*glm::length(cMatrix[0][1]) < glm::length(glm::dot(cMatrix[1][0]*axisThis[2], centersDistance) - glm::dot(cMatrix[2][0]*axisThis[1], centersDistance))) {
        return false;
    }

    if (extentsThis[1]*glm::length(cMatrix[2][1]) + extentsThis[2]*glm::length(cMatrix[1][1]) + extentsOther[0]*glm::length(cMatrix[0][2]) + extentsOther[2]*glm::length(cMatrix[0][0]) < glm::length(glm::dot(cMatrix[1][1]*axisThis[2], centersDistance) - glm::dot(cMatrix[2][1]*axisThis[1], centersDistance))) {
        return false;
    }

    if (extentsThis[1]*glm::length(cMatrix[2][2]) + extentsThis[2]*glm::length(cMatrix[1][2]) + extentsOther[0]*glm::length(cMatrix[0][1]) + extentsOther[1]*glm::length(cMatrix[0][0]) < glm::length(glm::dot(cMatrix[1][2]*axisThis[2], centersDistance) - glm::dot(cMatrix[2][2]*axisThis[1], centersDistance))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[2][0]) + extentsThis[2]*glm::length(cMatrix[0][0]) + extentsOther[1]*glm::length(cMatrix[1][2]) + extentsOther[2]*glm::length(cMatrix[1][1]) < glm::length(glm::dot(cMatrix[2][0]*axisThis[0], centersDistance) - glm::dot(cMatrix[0][0]*axisThis[2], centersDistance))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[2][1]) + extentsThis[2]*glm::length(cMatrix[0][1]) + extentsOther[0]*glm::length(cMatrix[1][2]) + extentsOther[2]*glm::length(cMatrix[1][0]) < glm::length(glm::dot(cMatrix[2][1]*axisThis[0], centersDistance) - glm::dot(cMatrix[0][1]*axisThis[2], centersDistance))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[2][2]) + extentsThis[2]*glm::length(cMatrix[0][2]) + extentsOther[0]*glm::length(cMatrix[1][1]) + extentsOther[1]*glm::length(cMatrix[1][0]) < glm::length(glm::dot(cMatrix[2][2]*axisThis[0], centersDistance) - glm::dot(cMatrix[0][2]*axisThis[2], centersDistance))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[1][0]) + extentsThis[1]*glm::length(cMatrix[0][0]) + extentsOther[1]*glm::length(cMatrix[2][2]) + extentsOther[2]*glm::length(cMatrix[2][1]) < glm::length(glm::dot(cMatrix[0][0]*axisThis[1], centersDistance) - glm::dot(cMatrix[1][0]*axisThis[0], centersDistance))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[1][1]) + extentsThis[1]*glm::length(cMatrix[0][1]) + extentsOther[0]*glm::length(cMatrix[2][2]) + extentsOther[2]*glm::length(cMatrix[2][0]) < glm::length(glm::dot(cMatrix[0][1]*axisThis[1], centersDistance) - glm::dot(cMatrix[1][1]*axisThis[0], centersDistance))) {
        return false;
    }

    if (extentsThis[0]*glm::length(cMatrix[1][2]) + extentsThis[1]*glm::length(cMatrix[0][2]) + extentsOther[0]*glm::length(cMatrix[2][1]) + extentsOther[1]*glm::length(cMatrix[2][0]) < glm::length(glm::dot(cMatrix[0][2]*axisThis[1], centersDistance) - glm::dot(cMatrix[1][2]*axisThis[0], centersDistance))) {
        return false;
    }

    if (movingObjects) {
        glm::vec3 w = velocityOther - velocityThis;

        glm::vec3 alpha {
            glm::dot(w, axisThis[0]),
            glm::dot(w, axisThis[1]),
            glm::dot(w, axisThis[2])
        };
        glm::vec3 beta {
            glm::dot(w, axisOther[0]),
            glm::dot(w, axisOther[1]),
            glm::dot(w, axisOther[2])
        };

        if (extentsThis[1]*glm::length(alpha[2]) + extentsThis[2]*glm::length(alpha[1]) + extentsOther[0]*glm::length(cMatrix[1][0]*alpha[2] - cMatrix[2][0]*alpha[1]) + extentsOther[1]*glm::length(cMatrix[1][1]*alpha[2] - cMatrix[2][1]*alpha[1]) + extentsOther[2]*glm::length(cMatrix[1][2]*alpha[2] - cMatrix[2][2]*alpha[1]) < glm::dot(axisThis[0], glm::cross(w, centersDistance))) {
            return false;
        }

        if (extentsThis[0]*glm::length(alpha[2]) + extentsThis[2]*glm::length(alpha[0]) + extentsOther[0]*glm::length(cMatrix[0][0]*alpha[2] - cMatrix[2][0]*alpha[0]) + extentsOther[1]*glm::length(cMatrix[0][1]*alpha[2] - cMatrix[2][1]*alpha[0]) + extentsOther[2]*glm::length(cMatrix[0][2]*alpha[2] - cMatrix[2][2]*alpha[0]) < glm::dot(axisThis[1], glm::cross(w, centersDistance))) {
            return false;
        }

        if (extentsThis[0]*glm::length(alpha[1]) + extentsThis[1]*glm::length(alpha[0]) + extentsOther[0]*glm::length(cMatrix[0][0]*alpha[1] - cMatrix[1][0]*alpha[0]) + extentsOther[1]*glm::length(cMatrix[0][1]*alpha[1] - cMatrix[1][1]*alpha[0]) + extentsOther[2]*glm::length(cMatrix[0][2]*alpha[1] - cMatrix[1][2]*alpha[0]) < glm::dot(axisThis[2], glm::cross(w, centersDistance))) {
            return false;
        }

        if (extentsThis[0]*glm::length(cMatrix[0][1]*beta[2] - cMatrix[0][2]*beta[1]) + extentsThis[1]*glm::length(cMatrix[1][1]*beta[2] - cMatrix[1][2]*beta[1]) + extentsThis[2]*glm::length(cMatrix[2][1]*beta[2] - cMatrix[2][2]*beta[1]) + extentsOther[1]*glm::length(beta[2]) + extentsOther[2]*glm::length(beta[1]) < glm::dot(axisOther[0], glm::cross(w, centersDistance))) {
            return false;
        }

        if (extentsThis[0]*glm::length(cMatrix[0][0]*beta[2] - cMatrix[0][2]*beta[0]) + extentsThis[1]*glm::length(cMatrix[1][0]*beta[2] - cMatrix[1][2]*beta[0]) + extentsThis[2]*glm::length(cMatrix[2][0]*beta[2] - cMatrix[2][2]*beta[0]) + extentsOther[0]*glm::length(beta[2]) + extentsOther[2]*glm::length(beta[0]) < glm::dot(axisOther[1], glm::cross(w, centersDistance))) {
            return false;
        }

        if (extentsThis[0]*glm::length(cMatrix[0][0]*beta[1] - cMatrix[0][1]*beta[0]) + extentsThis[1]*glm::length(cMatrix[1][0]*beta[1] - cMatrix[1][1]*beta[0]) + extentsThis[2]*glm::length(cMatrix[2][0]*beta[1] - cMatrix[2][1]*beta[0]) + extentsOther[0]*glm::length(beta[2]) + extentsOther[1]*glm::length(beta[0]) < glm::dot(axisOther[2], glm::cross(w, centersDistance))) {
            return false;
        }
    }

    return true;
}

/**
 * @brief Method returning the entity's model matrix
 * Method returning the entity's pre-computed model matrix
 * @return glm::mat4 The model matrix of the entity
 */
glm::mat4 Entity::getModelMatrix() const {
    return this->m_modelMatrix;
}

/**
 * @brief Method returning the entity's X axis
 * Method returning the entity's local X axis
 * @return glm::vec3 The in-world representation of the entity's local X axis
 */
glm::vec3 Entity::get_x_axis() const { return this->m_localX; }

/**
 * @brief Method returning the entity's Y axis
 * Method returning the entity's local Y axis
 * @return glm::vec3 The in-world representation of the entity's local Y axis
 */
glm::vec3 Entity::get_y_axis() const { return this->m_localY; }

/**
 * @brief Method returning the entity's Z axis
 * Method returning the entity's local Z axis
 * @return glm::vec3 The in-world representation of the entity's local Z axis
 */
glm::vec3 Entity::get_z_axis() const { return this->m_localZ; }

/**
 * @brief Method returning the entity's in-world position
 * Method returning the entity's in-world position
 * @return glm::vec3 The entity's in-world position
 */
glm::vec3 Entity::get_position() const { return this->m_position; }

/**
 * @brief Method returning the entity's in-world rotation
 * Method returning the entity's in-world rotation
 * @return glm::vec3 The entity's in-world rotation
 */
glm::quat Entity::get_rotation() const { return this->m_rotation; }

/**
 * @brief Method returning the entity's in-world velocity
 * Method returning the entity's in-world velocity
 * @return glm::vec3 The entity's in-world velocity
 */
glm::vec3 Entity::get_velocity(const float &deltaTime) const { return glm::vec3(0); }
