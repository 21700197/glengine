#include "RenderObject.hpp"

RenderObject::RenderObject()
{
    m_diffusemap = std::unique_ptr<Sampler>(new Sampler(0));
    m_normalmap = std::unique_ptr<Sampler>(new Sampler(1));
    m_specularmap = std::unique_ptr<Sampler>(new Sampler(2));
}

void RenderObject::draw()
{
    m_diffusemap->bind();
    m_normalmap->bind();
    m_specularmap->bind();

    for (RenderObjectPart &part : m_parts) {
        part.draw(m_diffusemap.get(), m_normalmap.get(), m_specularmap.get());
    }

    m_diffusemap->unbind();
    m_normalmap->unbind();
    m_specularmap->unbind();
}

void RenderObject::update(const glm::mat4 &proj, const glm::mat4 &view, const glm::mat4 &model)
{
    for (RenderObjectPart &part : m_parts) {
        part.update(proj, view, model);
    }
}

std::unique_ptr<RenderObject> RenderObject::createWavefrontInstance(const std::string & objname)
{
    std::unique_ptr<RenderObject> object(new RenderObject());
    object->loadWavefront(objname);
    return object;
}

void RenderObject::setDisableLightning(const bool &disableLightning) {
    program->bind();
    program->setUniform("disableLightning", disableLightning);
    program->unbind();
}

glm::vec3 RenderObject::get_dimensions() const {
    return this->dimensions;
}

void RenderObject::setProgramMaterial(std::shared_ptr<Program> & program, const SimpleMaterial & material) const
{
    program->bind();
    program->setUniform("lightsInWorld[0].direction", glm::normalize(glm::vec3(0, -1, 1)));
    program->setUniform("lightsInWorld[0].intensity", glm::vec3(0.7, 0.7, 0.7));
    program->setUniform("lightsInWorld[1].direction", glm::normalize(glm::vec3(0, 1, 0.5)));
    program->setUniform("lightsInWorld[1].intensity", glm::vec3(0.5, 0.5, 0.5));
    program->setUniform("lightsInWorld[2].direction", glm::normalize(glm::vec3(-1, 0, 1)));
    program->setUniform("lightsInWorld[2].intensity", glm::vec3(0.6, 0.6, 0.6));
    program->setUniform("material.ambient", material.ambient);
    program->setUniform("material.diffuse", material.diffuse);
    program->setUniform("material.specular", material.specular);
    program->setUniform("material.shininess", material.shininess);
    m_diffusemap->attachToProgram(*program, "material.colormap", Sampler::DoNotBind);
    m_normalmap->attachToProgram(*program, "material.normalmap", Sampler::DoNotBind);
    m_specularmap->attachToProgram(*program, "material.specularmap", Sampler::DoNotBind);
    program->unbind();
}

void RenderObject::loadWavefront(const std::string & objname) {
    ObjLoader objLoader(objname);
    const std::vector<SimpleMaterial> & materials = objLoader.materials();
    std::vector<glm::vec3> vertexPositions = objLoader.vertexPositions();
    const std::vector<glm::vec2> & vertexUVs = objLoader.vertexUVs();
    std::vector<glm::vec3> vertexNormals = objLoader.vertexNormals();
    std::vector<glm::vec3> vertexTangents = objLoader.vertexTangents();

    // Box creation
    glm::vec3 maxXYZ = vertexPositions[0];
    glm::vec3 minXYZ = vertexPositions[0];

    for (glm::vec3 position : vertexPositions) {
        if (maxXYZ.x < position.x) {
            maxXYZ.x = position.x;
        } else if (minXYZ.x > position.x) {
            minXYZ.x = position.x;
        }

        if (maxXYZ.y < position.y) {
            maxXYZ.y = position.y;
        } else if (minXYZ.y > position.y) {
            minXYZ.y = position.y;
        }

        if (maxXYZ.z < position.z) {
            maxXYZ.z = position.z;
        } else if (minXYZ.z > position.z) {
            minXYZ.z = position.z;
        }
    }

    this->dimensions = glm::vec3(std::max(std::abs(maxXYZ.x), std::abs(minXYZ.x)),
                                 std::max(std::abs(maxXYZ.y), std::abs(minXYZ.y)),
                                 std::max(std::abs(maxXYZ.z), std::abs(minXYZ.z)));

    std::cout << this->dimensions.x << " " << this->dimensions.y << " " << this->dimensions.z << std::endl;
    
    // set up the VBOs of the master VAO
    std::shared_ptr<VAO> vao(new VAO(4));
    vao->setVBO(0, vertexPositions);
    vao->setVBO(1, vertexUVs);
    vao->setVBO(2, vertexNormals);
    vao->setVBO(3, vertexTangents);

    size_t nbParts = objLoader.nbIBOs();
    for (size_t k = 0; k < nbParts; k++) {
        const std::vector<uint> & ibo = objLoader.ibo(k);
        if (ibo.size() == 0) {
            continue;
        }
        std::shared_ptr<VAO> vaoSlave;
        vaoSlave = vao->makeSlaveVAO();
        vaoSlave->setIBO(ibo);

        program = std::shared_ptr<Program>(new Program("shaders/simplemat.v.glsl", "shaders/simplemat.f.glsl"));
        const SimpleMaterial & material = materials[k];
        setProgramMaterial(program, material);
        
        Image<> colorMap = objLoader.image(material.diffuseTexName);
        std::shared_ptr<Texture> texture(new Texture(GL_TEXTURE_2D));
        texture->setData(colorMap);

        Image<> normalMap = objLoader.image(material.normalTexName);
        std::shared_ptr<Texture> ntexture(new Texture(GL_TEXTURE_2D));
        ntexture->setData(normalMap);

        Image<> specularMap = objLoader.image(material.specularTexName);
        std::shared_ptr<Texture> stexture(new Texture(GL_TEXTURE_2D));
        stexture->setData(specularMap);

        m_parts.emplace_back(vaoSlave, program, texture, ntexture, stexture);
    }

    m_diffusemap->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_diffusemap->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_diffusemap->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_diffusemap->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    m_normalmap->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_normalmap->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_normalmap->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_normalmap->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    m_specularmap->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_specularmap->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_specularmap->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_specularmap->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
}
