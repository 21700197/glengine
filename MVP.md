# `MVP.md` *(EN)*

## Team

<u>Team leader:</u> ∅
<u>Developper 1:</u> Benjamin CASTEL *(21700197)*
<u>Developper 2:</u> Justine MARTIN *(21909920)*
<u>Asset Designer :</u> Justine MARTIN *(21909920)*

## Game overview

<u>Goal :</u> Two space pilots fight each other to death in an asteroid-filled nebula.
<u>Gameplay :</u> Space "arcade"-ish "combat flight" simulator
<u>Controls :</u> keyboard for the two players :

- <u>Player 1</u> `W`/`S` for pitch, `A`/`D` for yaw, `Q`/`E` for roll, `Left_shift` for thrusting, `Space` for attacking
- <u>Player 2</u> `NUM_8`/`NUM_5` for pitch, `NUM_4`/`NUM_6` for yaw, `NUM_7`/`NUM_9` for roll, `NUM_ENTER` for thrusting, `NUM_0` for attacking
- Generally, `Escape` to quit, `L` to print a short debug log to the console, `N` to display hitboxes and `ENTER` to start a new game after the end of another one.

## Detailed Description

**(sp)Ace Combat** v.1.0 starts with a red and a blue spaceships, each controlled on the same keyboard by two human players.

In this asteroïd field, each player have the same objective : kill his oponent by shooting deadly, destructive missiles at him.

Piloting can be achieved in an arcade-ish ways by rotating the ship, while thrusting is achieved by pressing a simple button. Due to ships's inertial dampeners, a non-thrusting ship will stop, and a ship will always move forward when turning.

The console is written to when collisions with missiles happen (to keep track of the game). Some debug infos are also written when starting the game.

## Technical features

- OBBs with collision detection
- Basic physics for spaceships
- Skybox
- 3D models
- Semi-optimized rendering
- GUI

## Known Bugs / Missing features

- If two spaceships collide and one of them then rotates, it *may* result in the two ship being stuck. Often, this can be resolved by rotating both ships to untangle everything.
- You can go through the asteroïds.
- You can go out of the skybox (it's hot pink behind it by the way).
- The way health GUI is handled isn't that good (we'd like to implement a cropping system for textures).
