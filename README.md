# (sp)Ace Combat

## Présentation

**(sp)Ace Combat** (nom s'inspirant de la saga de simulateurs de combats aériens (terrestres cette fois) [Ace Combat](https://fr.wikipedia.org/wiki/Ace_Combat)) est notre production pour le projet de Synthèse d'Image (M1 Info IDM - Unicaen 2021-2022).

Il s'agit d'un jeu vidéo de type shooter multijoueur, mettant les joueurs dans un cadre de combat spatial, où ils peuvent contrôler chacun leur vaisseau sur le même clavier.

Une vidéo de démo est disponible [ici](spAceCombat%20trailer.mp4).

![](spAceCombat%20trailer.mp4)

## Architecture des dossiers

### Dépendances

Le projet est basé sur [glitter](https://drlsimon.github.io/glitter). Notre implémentation de glitter est située dans le dossier `glitter`. Le dossier [glitter/ext](glitter/ext) inclut nos seules dépendances : les librairies [glew](https://github.com/nigels-com/glew), [glfw](https://github.com/glfw/glfw), [glm](https://github.com/g-truc/glm), [stb](https://github.com/nothings/stb), [termcolor](https://github.com/ikalnytskyi/termcolor) et [tinyobjloader](https://github.com/tinyobjloader/tinyobjloader).

### Assets

La totalité des assets 3D sont situés dans le dossier [meshes](meshes). Ci-dessous se trouve un tableau récapitulant tous nos assets, indiquant leur provenance :

|                      .obj                        | Provenance                                                                              | Utilisé |
|:------------------------------------------------:|:----------------------------------------------------------------------------------------|--------:|
|     [Astéroïde](meshes/asteroid/asteroid.obj)    | Justine Martin & [3dtextures.me](https://3dtextures.me/2021/04/09/rock-040/)            |     Oui |
| [Vaisseau bleu](meshes/fighter/fighter_blue.obj) | [niko-3D-models sur itch.io](https://niko-3d-models.itch.io/free-sc-fi-spaceships-pack) |     Oui |
| [Vaisseau rouge](meshes/fighter/fighter_red.obj) | [niko-3D-models sur itch.io](https://niko-3d-models.itch.io/free-sc-fi-spaceships-pack) |     Oui |
|     [Cube](meshes/normalMappedCube/cube.obj)     | [glitter](glitter/meshes/normalMappedCube/cube.obj)                                     |     Non |
| [Palette](meshes/Pallet/Bswap_HPBake_Planks.obj) | [glitter](glitter/meshes/Pallet/Bswap_HPBake_Planks.obj)                                |     Non |
|      [Tron](meshes/Tron/TronLightCycle.obj)      | [glitter](glitter/meshes/Tron/TronLightCycle.obj)                                       |     Non |
|           [Capsule](meshes/capsule.obj)          | [glitter](glitter/meshes/capsule.obj)                                                   |     Non |
|       [Cornell Box](meshes/cornell_box.obj)      | [glitter](glitter/meshes/cornell_box.obj)                                               |     Non |
|       [Missile Bleu](meshes/laser_blue.obj)      | Justine Martin                                                                          |     Oui |
|       [Missile Rouge](meshes/laser_red.obj)      | Justine Martin                                                                          |     Oui |
|            [Skybox](meshes/skybox.obj)           | Justine Martin & [wwwtyro.net](https://tools.wwwtyro.net/space-3d/index.html)           |     Oui |
|          [unit_box](meshes/unit_box.obj)         | Benjamin Castel                                                                         |     Non |
|       [unit_box v2](meshes/unit_box_v2.obj)      | Benjamin Castel                                                                         |     Oui |

De plus, le dossier [meshes](meshes) contient des images. Voici leur tableau :

|                     image                     |                   Provenance                  | Utilisée |
|:---------------------------------------------:|:----------------------------------------------|---------:|
|        [capsule0](meshes/capsule0.jpg)        |     [glitter](glitter/meshes/capsule0.jpg)    |      Non |
|  [checkerboardNM](meshes/checkerboardNM.png)  |  [glitter](glitter/meshes/checkerboardNM.png) |      Non |
| [checkerBoardRGB](meshes/checkerboardRGB.png) | [glitter](glitter/meshes/checkerboardRGB.png) |      Non |
|            [draw](meshes/draw.png)            |                 Justine Martin                |      Oui |
|           [life1](meshes/life1.png)           |                 Justine Martin                |      Oui |
|           [life1](meshes/life2.png)           |                 Justine Martin                |      Oui |
|           [life1](meshes/life3.png)           |                 Justine Martin                |      Oui |
|            [lose](meshes/lose.png)            |                 Justine Martin                |      Oui |
|             [win](meshes/win.png)             |                 Justine Martin                |      Oui |

### Rapport et informations

Le rapport est présent sous forme non-compilée en tant que [fichier markdown](rapport/rapport.md), pouvant être compilé en `.pdf` avec [pandoc](https://github.com/jgm/pandoc). Un [script shell pour le compiler](rapport/compile_report.sh) comme nous l'avons prévu existe dans le même dossier [rapport](rapport).

D'autre part, ainsi que les consignes de rendus le demandent, la racine du dépôt contient un fichier [MVP.md](MVP.md), que nous rendons également disponible en [version française](MVP-fr.md).

## Code-source

Le code source est séparé en trois parties :

- Le dossier [include](include) contient tous les headers (fichiers `.hpp`).
- Le dossier [src](src) contient les fichiers d'implémentation (fichiers `.cpp`).
- le dossier [shaders](shaders) contient tous les codes-sources des *shaders* (fichiers `.glsl`).

## CMake

Notre environnement de compilation est assuré par le fichier [CMakeLists](CMakeLists.txt) à la racine du présent dépôt. Il est compatible avec toutes les versions de CMake supérieures à `3.0`, et impose l'utilisation du standard `C++11`.

L'environnement inclut également l'environnement `CMake` de `glitter`, et importe donc tous ses executables et dépendances.

Notre exécutable s'appelle `spAceCombat`.

## Téléchargement et compilation

Le projet contenant divers sous-modules, la commande de clonage `git` est :

```sh
git clone git@git.unicaen.fr:21700197/glengine.git --recurse-submodules
```

La commande de *compilation + lancement*, depuis la racine, que nous suggérons, est la suivante :

```sh
mkdir build && cd build
cmake .. && make && ./spAceCombat
```
