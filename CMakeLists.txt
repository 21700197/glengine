cmake_minimum_required (VERSION 3.0)

project(GLEngine)

set (CMAKE_CXX_STANDARD 11)
add_definitions(-Wall -Wextra)

add_subdirectory("glitter")

include_directories("include")

include_directories("glitter/ext/tinyobjloader"
		"glitter/src/"
		"glitter/ext/glew/include glitter/ext/glew/src"
		"glitter/ext/stb"
		"glitter/ext/glfw/include"
		"glitter/ext/glm"
		${OPENGL_INCLUDE_DIR})

add_executable(spAceCombat src/main.cpp src/RenderObject.cpp src/GuiElement.cpp src/RenderObjectPart.cpp src/GlApplication.cpp
               src/model/Spaceship.cpp src/model/Asteroid.cpp src/model/LifeGui.cpp src/model/Entity.cpp src/model/Missile.cpp)
target_link_libraries(spAceCombat utils ${GLFW3_LIBRARIES} ${GLEW_LIBRARIES})
