#pragma once 


#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include <string>
#include <memory>


#include "RenderObject.hpp"



/**
 * @brief Base class for every entities
 * Class inherited by any object present in the 3D world
 */
class Entity {
    protected:
        glm::mat4 m_modelMatrix; // Model matrix used to render the entity

        glm::vec3 m_position;    // Position point
        glm::quat m_rotation;    // Rotation quaternion
        glm::vec3 m_scale;       // Scale matrix, representing semi-scale on every axis

        glm::vec3 m_localX;      // Local X axis
        glm::vec3 m_localY;      // Local Y axis
        glm::vec3 m_localZ;      // Local Z axis


    public:
        Entity();
        virtual void update(const float &time);

        glm::mat4 getModelMatrix() const;
        glm::vec3 get_x_axis() const;
        glm::vec3 get_y_axis() const;
        glm::vec3 get_z_axis() const;

        glm::quat get_rotation() const;

        glm::vec3 get_position() const;
        virtual glm::vec3 get_velocity(const float &deltaTime) const;

        void move(glm::vec3 translation);
        void rotate(glm::quat rotation);
        void setScale(float scale);
        void setScale(glm::vec3 scales);

        glm::mat4 get_box_model_matrix(std::unique_ptr<RenderObject> &renderObject) const;
        bool check_box_intersection(const std::unique_ptr<RenderObject> &renderObjectThis,
                                    const Entity &other,
                                    const std::unique_ptr<RenderObject> &renderObjectOther,
                                    const bool &movingObjects=false,
                                    const glm::vec3 &velocityThis=glm::vec3(0),
                                    const glm::vec3 &velocityOther=glm::vec3(0)) const;
    protected:
        void computeModelMatrix(); 
};
