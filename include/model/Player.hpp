#pragma once


/**
 * @brief Player enum
 * Enum representing a player by it's all-letters number
 */
enum Player {
    one, // Player 1
    two  // Player 2
};
