#pragma once


#include "glm/glm.hpp"


#include "model/Entity.hpp"
#include "model/Missile.hpp"
#include "model/Player.hpp"


/**
 * @brief Class representing a spaceship controlled by a human player
 * Entity-derived spaceship class
 */
class Spaceship : public Entity {
    private:
        float        acceleration; // Float used as a scalar to be multiplied to local Z axis vector in order to move forward at a certain speed
        Player       playerNumber; // Player piloting the ship
        bool         thrusting;    // Boolean representing wether the spaceship is currenntly thrusting
        bool         blocked;      // Boolean representing wether the spaceship is currently blocked (colliding)
        float        fireCooldown; // Float representing current left countdown to shoot again
        unsigned int healthPoints; // Unsigned int containing the ship's remaining health points

    public:
        Spaceship(const Player &playerNumber, const glm::mat4 &modelWorld);

        const float        &get_acceleration()  const;
        const Player       &get_player_number() const;
        const bool         &is_thrusting()      const;
        const bool         &is_blocked()        const;
        const unsigned int &get_health_points() const;

        glm::vec3 get_velocity(const float &deltaTime) const override;

        void set_acceleration(const float &newAcceleration);
        void set_thrusting(const bool &newThrusting);
        
        void take_damage();

        void update(const float &deltaTime) override;

        std::shared_ptr<Missile> generate_missile();

        // TODO : REMOVE AND REPLACE
        void thrust(const float &deltaTime,
                    const std::unique_ptr<RenderObject> &renderObjectThis,
                    const Entity &otherShip,
                    const std::unique_ptr<RenderObject> &renderObjectOtherShip);
};
