# `MVP.md` *(FR)*

## Équipe

<u>Chef d'équipe :</u> ∅
<u>Développeur 1:</u> Benjamin CASTEL *(21700197)*
<u>Développeur 2:</u> Justine MARTIN *(21909920)*
<u>Asset Designer :</u> Justine MARTIN *(21909920)*

## Aperçu du jeu

<u>Objectif :</u> Deux pilotes de vaisseaux spatiaux combattent jusqu'à la mort dans une nébuleuse pleine d'astéroïdes
<u>Gameplay :</u> Jeu de simulation de combat spatial dans un style arcade
<u>Contrôles :</u> Au clavier pour les deux joueurs :

- <u>Joueur 1</u> `Z`/`S` pour le pitch, `Q`/`D` pour le yaw, `A`/`E` pour le roll, `Left_shift` pour accélérer, `Espace` pour attaquer
- <u>Player 2</u> `NUM_8`/`NUM_5` pour le pitch, `NUM_4`/`NUM_6` pour le yaw, `NUM_7`/`NUM_9` pour le roll, `NUM_ENTER` pour accélérer, `NUM_0` pour attaquer
- De manière générale, `Echap` pour quitter, `L` pour afficher un court message de debug dans la console, `N` pour afficher les boîtes de collision des entités et `ENTRÉE` pour démarrer une nouvelle partie lorsqu'elle est terminée.

## Description détaillée

**(sp)Ace Combat** v.1.0 démarre avec des vaisseaux spatiaux rouge et bleu, chacun contrôlé par un même clavier partagé par les deux joueurs humains.

Dans ce champ d'astéroïde, chaque joueur possède le même objectif : tuer son opposant en lui tirant de meurtriers et destructeurs missiles colorés.

Le pilotage se fait dans un style très "arcade", en pivotant le vaisseau, tandis que l'accélération est réalisée par la simple pression d'un bouton. Grace aux amortisseurs inertiels des vaisseaux, un vaisseau qui ne se propulse pas ralentira et un vaisseau avancera toujours dans la direction dans laquelle il est tourné.

Le jeu écrit dans la console lorsqu'une collision survient avec un missile, pour "relire" une partie. Des infos de debug sont également écrites au début du jeu.

## Fonctionnalités techniques

- Détection de collision d'OBBs
- Physique pour les vaisseaux spatiaux
- Skybox
- Modèles 3D
- Rendu semi-optimisé
- GUI

## Bugs connus / Fonctionnalités manquantes

- Si deux vaisseaux spatiaux entrent en collision et que l'un d'eux pivote, il peut arriver que les deux vaisseaux se retrouvent bloqués. Souvent, il est possible de résoudre ce souci en pivotant les deux vaisseaux pour les "démêler".
- Il est possible de passer au travers des astéroïdes.
- Il est possible de sortir de la Skybox (fun fact : le fond est tout rose derrière).
- La manière dont le GUI fonctionne n'est pas parfaite (nous aurions voulu implémenter un système de "cropping" pour ne sélectionner qu'une partie d'une texture).
