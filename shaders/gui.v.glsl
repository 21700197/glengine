#version 410

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec2 vertexUV;

uniform mat4 P;
uniform mat4 M;

out vec2 uv;

void main()
{
  gl_Position = P * M * vec4(vertexPosition, 0, 1);

  uv = vertexUV;
}
